필수 설치 목록
1: 1.QXI_All ->2.SAS관련 -> CdrvNT-30.0.4.1 -> CdrvNTWDMSetup 실행
2: 기본적인 설치 경로는 C:\Program Files (x86)\CdrvNTW 이다
이곳에서 CdrvNTWDMDriverInstall 30.0.4.1 실행하여 드라이버설치
설치 완료하면 C:\CdrvNT 가 생성된다.

3: CdrvNT-30.0.4.1 폴더안의 CdrvNTWDMOptions에서
[DEVICE1]

; Use the following to configure DEVICE1 by address/irq

ADDRESS=5300
IRQ=23

PORT1을 보고 ADDRESS와 IRQ를 맞춰준다

그후 이텍스트파일을 C:\CdrvNT 폴더안에 복붙

SAS와 클라 연동 테스트 시작